cmake_minimum_required(VERSION 3.2 FATAL_ERROR)

if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Debug CACHE STRING "Choose the type of build (Debug or Release)" FORCE)
endif()

project(testSFML VERSION 0.1.0 LANGUAGES CXX C)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

cmake_policy(SET CMP0072 NEW)

find_package(OpenGL REQUIRED)
find_package(GLEW REQUIRED)
find_package(SFML 2.5 REQUIRED system window graphics network audio)

add_library(g3d2 INTERFACE)
target_include_directories(g3d2 INTERFACE include/ OpenGL::GL ${GLEW_INCLUDE_DIRS} ${SFML_INCLUDE_DIRS})
target_link_libraries(g3d2 INTERFACE OpenGL::GL ${GLEW_LIBRARIES} sfml-graphics)

#file(GLOB testSFML_SRC "tests/main.cpp")

add_executable(test_particles tests/particles.cpp)
target_link_libraries(test_particles g3d2)