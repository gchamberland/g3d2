#include <SFML/Window.hpp>

#include <iostream>
#include <chrono>

#include <GL/glew.h>

#include <g3d2/gl_buffer.h>
#include <g3d2/gl_program.h>
#include <g3d2/matrix.h>
#include <g3d2/vector.h>
#include <g3d2/texture.h>
#include <g3d2/particle_emitter.h>

using Time = std::chrono::time_point<std::chrono::system_clock>;
using Duration = std::chrono::duration<double>;
using Clock = std::chrono::system_clock;

int main()
{
    Time lastTime = std::chrono::system_clock::now();
    // crée la fenêtre
    sf::Window window(sf::VideoMode(800, 600), "OpenGL", sf::Style::Default, sf::ContextSettings(32));
    window.setVerticalSyncEnabled(true);

    sf::ContextSettings settings = window.getSettings();

    glewInit();

    std::cout << "depth bits:" << settings.depthBits << std::endl;
    std::cout << "stencil bits:" << settings.stencilBits << std::endl;
    std::cout << "antialiasing level:" << settings.antialiasingLevel << std::endl;
    std::cout << "version:" << settings.majorVersion << "." << settings.minorVersion << std::endl;

    // chargement des ressources, initialisation des états OpenGL, ...

    G3D::GLTexture2D particleTexture("../particle.png");
    G3D::GLTexture2D particleTexture2("../particle2.png");

    std::map<G3D::GLTexture2D *, float> textures;
    textures[&particleTexture] = 0.5f;
    textures[&particleTexture2] = 0.5f;

    G3D::ParticleEmitter::init();

    G3D::ParticleEmitter emitter(
        100,
        3.0f, 0.5f, 2.0f,
        1.5f, 4.0f, 0.1f,
        5.0f,
        G3D::Color(173,216,230), G3D::Color(0, 0, 255),
        textures,
        0.1f, 1.0f, 1.0f
    );

    G3D::Matrix4f viewMatrix = G3D::Matrix4f::translation(0.0f, 0.0f, -5.0f);
    G3D::Matrix4f modelMatrix = G3D::Matrix4f::rotation(-90.0f, 1, 0, 0);

    emitter.setProjectionMatrix(G3D::Matrix4f::perspective(800, 600, 60, 0.1f, 100));

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    // la boucle principale
    bool running = true;
    while (running)
    {
        // gestion des évènements
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
            {
                // on stoppe le programme
                running = false;
            }
            else if (event.type == sf::Event::Resized)
            {
                // on ajuste le viewport lorsque la fenêtre est redimensionnée
                glViewport(0, 0, event.size.width, event.size.height);
            }
                
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
                modelMatrix = modelMatrix * G3D::Matrix4f::rotation(10.0f, 0.0f, 1.0f, 0.0f);
            }

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
                modelMatrix = modelMatrix * G3D::Matrix4f::rotation(-10.0f, 0.0f, 1.0f, 0.0f);
            }

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
                viewMatrix = viewMatrix * G3D::Matrix4f::translation(0.0f, 0.0f, -0.05f);
            }

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
                viewMatrix = viewMatrix * G3D::Matrix4f::translation(0.0f, 0.0f, 0.05f);
            }

        }

        float delta = std::chrono::duration<float>(std::chrono::system_clock::now() - lastTime).count();
        lastTime = std::chrono::system_clock::now();

        // effacement les tampons de couleur/profondeur
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        emitter.update(delta);

        emitter.setViewMatrix(viewMatrix);
        emitter.setModelMatrix(modelMatrix);

        emitter.draw();

        // termine la trame courante (en interne, échange les deux tampons de rendu)
        window.display();
    }

    // libération des ressources...

    return 0;
}
