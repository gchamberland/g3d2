#ifndef COLOR_H
#define COLOR_H

#include <cstdint>

#include "math.h"

namespace G3D {
	class Color {

		public:
			/* math adapted from: http://www.rapidtables.com/convert/color/rgb-to-hsl.htm
			 * reasonably optimized for speed, without going crazy */
			static void rgb_to_hsv(int r, int g, int b, float *r_h, float *r_s, float *r_v) {
				float rp, gp, bp, cmax, cmin, delta;
				int cmaxwhich, cminwhich;

				rp = ((float) r) / 255;
				gp = ((float) g) / 255;
				bp = ((float) b) / 255;

				cmax = rp;
				cmaxwhich = 0; /* faster comparison afterwards */
				if (gp > cmax) { cmax = gp; cmaxwhich = 1; }
				if (bp > cmax) { cmax = bp; cmaxwhich = 2; }
				cmin = rp;
				cminwhich = 0;
				if (gp < cmin) { cmin = gp; cminwhich = 1; }
				if (bp < cmin) { cmin = bp; cminwhich = 2; }

				delta = cmax - cmin;

				/* HUE */
				if (delta == 0) {
					*r_h = 0;
				} else {
					switch (cmaxwhich) {
					case 0: /* cmax == rp */
						*r_h = 60 * (fmod ((gp - bp) / delta, 6.0f));
					break;

					case 1: /* cmax == gp */
						*r_h = 60 * (((bp - rp) / delta) + 2);
					break;

					case 2: /* cmax == bp */
						*r_h = 60 * (((rp - gp) / delta) + 4);
					break;
				}
				if (*r_h < 0)
					*r_h += 360;
				}

				*r_v = cmax;

				if (cmax == 0) {
					*r_s = 0;
				} else {
					*r_s = delta / cmax;
				}
			}

			static void rgb_to_hsv(Color color, float *r_h, float *r_s, float *r_v) {
				rgb_to_hsv((int)color.r, (int)color.g, (int)color.b, r_h, r_s, r_v);
			}

			static void hsv_to_rgb(float h, float s, float v, float *r_r, float *r_g, float *r_b) {
				if (h > 360)
				h -= 360;
				if (h < 0)
				h += 360;
				h = Math::clamp(h, 0.0f, 360.0f);
				s = Math::clamp(s, 0.0f, 1.0f);
				v = Math::clamp(v, 0.0f, 1.0f);
				float c = v * s;
				float x = c * (1.0f - std::abs(std::fmod ((h / 60.0f), 2.0f) - 1.0f));
				float m = v - c;
				float rp, gp, bp;
				int a = (int) h / 60;


				switch (a) {
					case 0:
						rp = c;
						gp = x;
						bp = 0;
					break;

					case 1:
						rp = x;
						gp = c;
						bp = 0;
					break;

					case 2:
						rp = 0;
						gp = c;
						bp = x;
					break;

					case 3:
						rp = 0;
						gp = x;
						bp = c;
					break;

					case 4:
						rp = x;
						gp = 0;
						bp = c;
					break;

					default: // case 5:
						rp = c;
						gp = 0;
						bp = x;
					break;
				}

				*r_r = (rp + m);
				*r_g = (gp + m);
				*r_b = (bp + m);
			}

			static void hsv_to_rgb(float h, float s, float v, int *r_r, int *r_g, int *r_b) {
				float r, g, b;
				hsv_to_rgb(h, s, v, &r, &g, &b);
				*r_r = (int) r * 255;
				*r_g = (int) g * 255;
				*r_b = (int) b * 255;
			}

			static Color hsv_to_rgb (float h, float s, float v) {
				int r, g, b;
				hsv_to_rgb(h, s, v, &r, &g, &b);
				return Color(r, g, b);
			}

			static Color lerp(Color a, Color b, float t) {
				float hsva[3];
		        float hsvb[3];
		        rgb_to_hsv(a, hsva, hsva+1, hsva+2);
		        rgb_to_hsv(b, hsvb, hsvb+1, hsvb+2);
		        hsva[0] = Math::lerp(hsva[0], hsvb[0], t);
		        hsva[1] = Math::lerp(hsva[1], hsvb[1], t);
		        hsva[2] = Math::lerp(hsva[2], hsvb[2], t);
		        return hsv_to_rgb(hsva[0], hsva[1], hsva[2]);
			}

			static void lerp(Color c1, Color c2, float t, float *r, float *g, float *b) {
				float hsva[3];
		        float hsvb[3];
		        rgb_to_hsv(c1, hsva, hsva+1, hsva+2);
		        rgb_to_hsv(c2, hsvb, hsvb+1, hsvb+2);
		        hsva[0] = Math::lerp(hsva[0], hsvb[0], t);
		        hsva[1] = Math::lerp(hsva[1], hsvb[1], t);
		        hsva[2] = Math::lerp(hsva[2], hsvb[2], t);
		        hsv_to_rgb(hsva[0], hsva[1], hsva[2], r, g, b);
			}

			uint8_t r, g, b, a;

			Color() {}

			Color(uint8_t r, uint8_t g, uint8_t b, uint8_t a = 255)
				:r(r), g(g), b(b), a(a) {}
	};
};

#endif