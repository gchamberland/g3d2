#ifndef RANDOM_H
#define RANDOM_H

#include "math.h"

#include <random>

namespace G3D {

	class Random {
		private:
			std::random_device rd;
		    std::mt19937 engine;

		public:
			Random() {
				engine = std::mt19937(rd());
			}

			float range(float min, float max) {
				return std::uniform_real_distribution<float>(min, max)(engine);
			}

			int range(int min, int max) {
				return std::uniform_int_distribution<int>(min, max)(engine);
			}

			int nextInt() {
				return std::uniform_int_distribution<int>()(engine);
			}

			int nextInt(int max) {
				return std::uniform_int_distribution<int>(0, max)(engine);
			}

			float nextFloat() {
				return std::uniform_real_distribution<float>()(engine);
			}
	};
	
};

#endif