#ifndef PARTICLE_EMITTER
#define PARTICLE_EMITTER

#include "color.h"
#include "vector.h"
#include "matrix.h"
#include "texture.h"
#include "gl_program.h"
#include "gl_buffer.h"
#include "random.h"

namespace G3D {
	
	class ParticleEmitter {
	    private:
	    	static constexpr const char *vertexShader =
	    		"#version 330\n"
	            "uniform mat4 mModel;"
	            "uniform mat4 mView;"
	            "uniform mat4 mProjection;"
	            "uniform vec3 vParticlePosition;"
	            "uniform vec2 vParticleSize;"

	            "attribute vec2 vPosition;"
	            "attribute vec2 vTexCoord;"

	            "varying vec2 texCoord;"

	            "void main() {"
	                "vec4 cameraRight_world = vec4(mView[0][0], mView[1][0], mView[2][0], 0.0);"
	                "vec4 cameraUp_world = vec4(mView[0][1], mView[1][1], mView[2][1], 0.0);"

	                "vec4 particlePosition_world = mModel * vec4(vParticlePosition, 1.0);"

	                "vec4 vertexPosition ="
	                    "particlePosition_world"
	                    "+ cameraRight_world * vPosition.x * vParticleSize.x"
	                    "+ cameraUp_world * vPosition.y * vParticleSize.y;"

	                "texCoord = vTexCoord;"
	                "gl_Position = mProjection * mView * vertexPosition;"
	            "} \n";

	    	static constexpr const char *fragmentShader =
	    		"#version 330\n"
	            "uniform mediump vec4 vColor;"
	            "uniform sampler2D sTexture;"
	            "uniform bool iPremultiply;"

	            "varying mediump vec2 texCoord;"

	            "void main() {"
	                "mediump vec4 texColor = texture2D(sTexture, texCoord);"

	                "if (iPremultiply) {"
	                    "texColor.rgb *= texColor.a;"
	                "}"

	                "gl_FragColor = texColor * vColor;"
	            "}\n";

	    	struct Particle {
	        	Vector3f position;
	    		Vector3f speed;
	        	float color[3];
	        	float life;
	        	int textureIndex;

	        	Particle() {
	        		life = -1.0f;
	        	}

	        	Particle(int textureIndex) :Particle() {
	        		this->textureIndex = textureIndex;
	        	}
	    	};

	    	static GLProgram & program() { static GLProgram program; return program; }
	    	static GLElementBuffer & indexBuffer() { static GLElementBuffer indexBuffer; return indexBuffer; }
	    	static GLVertexBuffer & vertexBuffer() { static GLVertexBuffer vertexBuffer; return vertexBuffer; }
	    	static GLVertexBuffer & texCoordBuffer() { static GLVertexBuffer texCoordBuffer; return texCoordBuffer; }
	    	static Random & rng() { static Random rng; return rng; }

		    Matrix4f modelMatrix;
		    Matrix4f viewMatrix;
		    Matrix4f projectionMatrix;

		    std::vector<Particle> particles;
		    std::vector<GLTexture2D *> textures;

		    float maxLife;
		    float minLife;
		    float deathSpeed;
		    float minSpeed;
		    float maxSpeed;
		    float drag;
		    float minBias, maxBias;
		    Color startColor;
		    Color endColor;

		    float radius;

		    float particleSize[2];

		    void createParticles(size_t particleCount, const std::map<GLTexture2D *, float> &textureMap) {
		        std::vector<unsigned char> textureDistribution;
		        unsigned char textureIndex = 0;

		        for (const auto &entry : textureMap) {
		            textures.push_back(entry.first);
		            int currentDistribution = (int) (entry.second * 100.0f);

		            for (int i=0; i<currentDistribution; i++) {
		                textureDistribution.push_back(textureIndex);
		            }

		            textureIndex++;
		        }

		        for (int i = (int) textureDistribution.size() - 1; i > 0; i--)
		        {
		            int index = rng().nextInt(i + 1);
		            unsigned char a = textureDistribution[index];
		            textureDistribution[index] = textureDistribution[i];
		            textureDistribution[i] = a;
		        }

		        for (int i=0; i<particleCount; i++) {
		            particles.emplace_back(textureDistribution[rng().nextInt(99)]);
		        }
		    }

		public:

			static void init() {
		        program() = GLProgram(vertexShader, fragmentShader);

		        unsigned short indices[] = { 0, 1, 2, 2, 3, 0 };
		        indexBuffer() = GLElementBuffer(indices, 6);

		        float positions[] = {
	                -0.5f, -0.5f,
	                 0.5f, -0.5f,
	                 0.5f,  0.5f,
	                -0.5f,  0.5f
		        };

		        vertexBuffer() = GLVertexBuffer(positions, 8);

		        unsigned short texCoords[] {
	                0, 0,
	                1, 0,
	                1, 1,
	                0, 1
	            };

		        texCoordBuffer() = GLVertexBuffer(texCoords, 8);
		    }

		    ParticleEmitter(
		    	size_t particleCount,
				float maxLife, float minLife, float deathSpeed,
				float minSpeed, float maxSpeed, float drag,
				float bias,
				Color startColor, Color endColor,
				const std::map<GLTexture2D *, float> &textures,
				float radius, float particleSizeX, float particleSizeY
			) {

		        float texturesProbabilityCount = 0;

		        for (const auto &entry : textures) {
		            texturesProbabilityCount += entry.second;
		        }

		        if (texturesProbabilityCount != 1.0f) {
		            throw std::runtime_error("Sum of textures probabilities must equals 1.");
		        }

		        createParticles(particleCount, textures);

		        this->maxLife = maxLife;
		        this->minLife = minLife;
		        this->deathSpeed = deathSpeed;
		        this->minSpeed = minSpeed;
		        this->maxSpeed = maxSpeed;
		        this->drag = drag;
		        this->minBias = -bias / 4.0f;
		        this->maxBias = bias / 4.0f;
		        this->startColor = startColor;
		        this->endColor = endColor;
		        this->radius = radius;
		        this->particleSize[0] = particleSizeX;
		        this->particleSize[1] = particleSizeY;
		    }

		    void setModelMatrix(const Matrix4f &matrix) {
		        this->modelMatrix = matrix;
		    }

		    void setViewMatrix(const Matrix4f &matrix) {
		        this->viewMatrix = matrix;
		    }

		    void setProjectionMatrix(const Matrix4f &matrix) {
		        this->projectionMatrix = matrix;
		    }

		    void update(float deltaTime) {
		        float deathAmount = deathSpeed * deltaTime;
		        float dragAmount = drag * deltaTime;

		        for (Particle &particle : particles) {
		            if (particle.life < 0) {
		                particle.life = rng().range(minLife, maxLife);

		                particle.speed[0] = rng().range(minBias, maxBias);
		                particle.speed[1] = rng().range(minSpeed, maxSpeed);
		                particle.speed[2] = rng().range(minBias, maxBias);

		                float t = rng().range(0.0f, 2.0f * (float) M_PI);
		                float r = (float) std::sqrt(rng().nextFloat()) * radius;
		                float x = r * (float) std::cos(t);
		                float z = r * (float) std::sin(t);

		                particle.position[0] = x;
		                particle.position[1] = 0;
		                particle.position[2] = z;
		            } else {
		                particle.life -= deathAmount;
		                particle.position[0] += particle.speed[0] * deltaTime;
		                particle.position[1] += particle.speed[1] * deltaTime;
		                particle.position[2] += particle.speed[2] * deltaTime;
		                particle.speed[0] -= dragAmount;
		                particle.speed[1] -= dragAmount;
		                particle.speed[2] -= dragAmount;
		            }

		            float lifeProportion = particle.life / maxLife;
		            Color::lerp(endColor, startColor, lifeProportion,
		            	&particle.color[0], &particle.color[1], &particle.color[2]
	            	);

	            	//alpha premultiplying
	            	particle.color[0] *= lifeProportion;
	            	particle.color[1] *= lifeProportion;
	            	particle.color[2] *= lifeProportion;
		        }
		    }

		    void draw() {
		        program().use();

		        glEnable(GL_BLEND);
		        glBlendFunc(GL_ONE, GL_ONE);
		        glDepthMask(false);

		        glEnableVertexAttribArray(program().getAttribut("vTexCoord").location);
		        texCoordBuffer().bind();
		        glVertexAttribPointer(program().getAttribut("vTexCoord").location, 2, GL_UNSIGNED_SHORT, false, 0, 0);

		        glEnableVertexAttribArray(program().getAttribut("vPosition").location);
		        vertexBuffer().bind();
		        glVertexAttribPointer(program().getAttribut("vPosition").location, 2, GL_FLOAT, false, 0, 0);

		        glUniform2fv(program().getUniform("vParticleSize").location, 1, particleSize);

		        glUniformMatrix4fv(program().getUniform("mModel").location, 1, false, modelMatrix.data());
		        glUniformMatrix4fv(program().getUniform("mView").location, 1, false, viewMatrix.data());
		        glUniformMatrix4fv(program().getUniform("mProjection").location, 1, false, projectionMatrix.data());

		        for (GLTexture2D *texture : textures) {
		            texture->bind();
		        }

		        indexBuffer().bind();

		        for (Particle particle : particles) {

		        	if (particle.life > 0.0f) {
		            	GLTexture2D *texture = textures[particle.textureIndex];

			            glUniform4fv(program().getUniform("vColor").location, 1, particle.color);
			            glUniform3fv(program().getUniform("vParticlePosition").location, 1, particle.position.data());
			            glUniform1i(program().getUniform("sTexture").location, texture->getTextureSlotIndex());
			            glUniform1i(program().getUniform("iPremultiply").location, 1);

			            glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0);
		        	}
		        }

		        indexBuffer().unbind();

		        for (GLTexture2D *texture : textures) {
		            texture->unbind();
		        }

		        vertexBuffer().unbind();

		        glDisableVertexAttribArray(program().getAttribut("vPosition").location);

		        texCoordBuffer().unbind();

		        glDisableVertexAttribArray(program().getAttribut("vTexCoord").location);

		        glDepthMask(true);
		        glDisable(GL_BLEND);
		    }
	};
};

#endif