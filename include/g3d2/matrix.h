#ifndef MATRIX_H
#define MATRIX_H

#include "math.h"

#include <algorithm>
#include <stdexcept>

namespace G3D {

	template <typename TYPE>
	class Matrix4 {

		private:
			TYPE m[16];

		public:
			static Matrix4<TYPE> identity() {
				return Matrix4<TYPE>(
					1, 0, 0, 0,
					0, 1, 0, 0,
					0, 0, 1, 0,
					0, 0, 0, 1
				);
			}

			static Matrix4<TYPE> perspective(TYPE width, TYPE height, TYPE fov, TYPE near, TYPE far) {
				TYPE fovRad = Math::degreeToRadian(fov),
					f = 1 / std::tan(fovRad / 2),
					ar = width / height,
					rangeInv = 1 / (near - far);

				return Matrix4<TYPE>(
					f / ar, 	0, 		0, 							0,
					0, 			f, 		0, 							0,
					0, 			0, 		(near + far) * rangeInv,   -1,
					0, 			0, 		near * far * rangeInv * 2, 	0
				);
			}

			static Matrix4<TYPE> ortho(TYPE left, TYPE right, TYPE top, TYPE bottom, TYPE near, TYPE far) {
				return Matrix4<TYPE>(
					2/(right-left), 0, 				0, 				0,
					0, 				2/(top-bottom), 0, 	    		0,
					0, 				0, 			   2/(near-far),    0,
					(left+right)/(left-right), (bottom+top)/(bottom-top), (near+far)/(near-far), 1
				);
			}

			static Matrix4<TYPE> translation(TYPE xT, TYPE yT, TYPE zT) {
				return Matrix4<TYPE>(
					 1,  0,  0, 0,
					 0,  1,  0, 0,
					 0,  0,  1, 0,
					xT, yT, zT, 1
				);
			}

			static Matrix4<TYPE> rotation(TYPE a, TYPE x, TYPE y, TYPE z) {
				TYPE aRad = Math::degreeToRadian(a), 
					c = std::cos(aRad),
					s = std::sin(aRad),
					t = 1 - c;

				return Matrix4<TYPE>(
					t*x*x + c, t*x*y + z*s, t*x*z - y*s, 0,
					t*x*y - z*s, t*y*y + c, t*y*z + x*s, 0,
					t*x*z + y*s, t*y*z - x*s, t*z*z + c, 0,
					0, 			 0, 		  0, 		 1
				);
			}

			static Matrix4<TYPE> scale(TYPE x, TYPE y, TYPE z) {
				return Matrix4<TYPE>(
					x, 0, 0, 0,
					0, y, 0, 0,
					0, 0, z, 0,
					0, 0, 0, 1
				);
			}

			Matrix4() {

			}

			Matrix4(const Matrix4<TYPE> &other) {
				std::copy(&other.m[0], &other.m[16], &m[0]);
			}

			Matrix4(
				TYPE e00, TYPE e01, TYPE e02, TYPE e03,
				TYPE e10, TYPE e11, TYPE e12, TYPE e13,
				TYPE e20, TYPE e21, TYPE e22, TYPE e23,
				TYPE e30, TYPE e31, TYPE e32, TYPE e33
			) {
				m[0] = e00; m[1] = e01; m[2] = e02; m[3] = e03;
				m[4] = e10; m[5] = e11; m[6] = e12; m[7] = e13;
				m[8] = e20; m[9] = e21; m[10] = e22; m[11] = e23;
				m[12] = e30; m[13] = e31; m[14] = e32; m[15] = e33;
			}

			TYPE &operator[](std::size_t index) {
				return m[index];
			}

			const TYPE &operator[](std::size_t index) const {
				return m[index];
			}

			TYPE &operator()(std::size_t rowIndex, std::size_t colIndex) {
				return m[rowIndex * 4 + colIndex];
			}

			const TYPE &operator()(std::size_t rowIndex, std::size_t colIndex) const {
				return m[rowIndex * 4 + colIndex];
			}

			Matrix4<TYPE> operator+(const Matrix4<TYPE> &b) {
				return Matrix4<TYPE>(
					m[0] + b.m[0], m[1] + b.m[1], m[2] + b.m[2], m[3] + b.m[3],
					m[4] + b.m[4], m[5] + b.m[5], m[6] + b.m[6], m[7] + b.m[7],
					m[8] + b.m[8], m[9] + b.m[9], m[10] + b.m[10], m[11] + b.m[11],
					m[12] + b.m[12], m[13] + b.m[13], m[14] + b.m[14], m[15] + b.m[15]
				);
			}

			Matrix4<TYPE> operator-(const Matrix4<TYPE> &b) {
				return Matrix4<TYPE>(
					m[0] - b.m[0], m[1] - b.m[1], m[2] - b.m[2], m[3] - b.m[3],
					m[4] - b.m[4], m[5] - b.m[5], m[6] - b.m[6], m[7] - b.m[7],
					m[8] - b.m[8], m[9] - b.m[9], m[10] - b.m[10], m[11] - b.m[11],
					m[12] - b.m[12], m[13] - b.m[13], m[14] - b.m[14], m[15] - b.m[15]
				);
			}

			Matrix4<TYPE> operator*(const Matrix4<TYPE> &other) {
				TYPE a = m[0], b = m[1], c = m[2], d = m[3];
				Matrix4<TYPE> result;
					
				result.m[0] = a*other.m[0] + b*other.m[4] + c*other.m[8]  + d*other.m[12];
				result.m[1] = a*other.m[1] + b*other.m[5] + c*other.m[9]  + d*other.m[13];
				result.m[2] = a*other.m[2] + b*other.m[6] + c*other.m[10] + d*other.m[14];
				result.m[3] = a*other.m[3] + b*other.m[7] + c*other.m[11] + d*other.m[15];

				a = m[4]; b = m[5]; c = m[6]; d = m[7];

				result.m[4] = a*other.m[0] + b*other.m[4] + c*other.m[8]  + d*other.m[12];
				result.m[5] = a*other.m[1] + b*other.m[5] + c*other.m[9]  + d*other.m[13];
				result.m[6] = a*other.m[2] + b*other.m[6] + c*other.m[10] + d*other.m[14];
				result.m[7] = a*other.m[3] + b*other.m[7] + c*other.m[11] + d*other.m[15];

				a = m[8]; b = m[9]; c = m[10]; d = m[11];

				result.m[8]  = a*other.m[0] + b*other.m[4] + c*other.m[8]  + d*other.m[12];
				result.m[9]  = a*other.m[1] + b*other.m[5] + c*other.m[9]  + d*other.m[13];
				result.m[10] = a*other.m[2] + b*other.m[6] + c*other.m[10] + d*other.m[14];
				result.m[11] = a*other.m[3] + b*other.m[7] + c*other.m[11] + d*other.m[15];

				a = m[12]; b = m[13]; c = m[14]; d = m[15];

				result.m[12] = a*other.m[0] + b*other.m[4] + c*other.m[8]  + d*other.m[12];
				result.m[13] = a*other.m[1] + b*other.m[5] + c*other.m[9]  + d*other.m[13];
				result.m[14] = a*other.m[2] + b*other.m[6] + c*other.m[10] + d*other.m[14];
				result.m[15] = a*other.m[3] + b*other.m[7] + c*other.m[11] + d*other.m[15];

				return result;
			}

			/*
			Vector4<TYPE> operator*(const Vector4<TYPE> &v) {
				//const float &a = v.x(), &b = 
			}
			*/

			Matrix4<TYPE> inverse() {
				Matrix4<TYPE> inv;
				TYPE det;
				unsigned char i;

			    inv.m[0] = m[5]  * m[10] * m[15] - 
			             m[5]  * m[11] * m[14] - 
			             m[9]  * m[6]  * m[15] + 
			             m[9]  * m[7]  * m[14] +
			             m[13] * m[6]  * m[11] - 
			             m[13] * m[7]  * m[10];

			    inv.m[4] = -m[4]  * m[10] * m[15] + 
			              m[4]  * m[11] * m[14] + 
			              m[8]  * m[6]  * m[15] - 
			              m[8]  * m[7]  * m[14] - 
			              m[12] * m[6]  * m[11] + 
			              m[12] * m[7]  * m[10];

			    inv.m[8] = m[4]  * m[9] * m[15] - 
			             m[4]  * m[11] * m[13] - 
			             m[8]  * m[5] * m[15] + 
			             m[8]  * m[7] * m[13] + 
			             m[12] * m[5] * m[11] - 
			             m[12] * m[7] * m[9];

			    inv.m[12] = -m[4]  * m[9] * m[14] + 
			               m[4]  * m[10] * m[13] +
			               m[8]  * m[5] * m[14] - 
			               m[8]  * m[6] * m[13] - 
			               m[12] * m[5] * m[10] + 
			               m[12] * m[6] * m[9];

			    inv.m[1] = -m[1]  * m[10] * m[15] + 
			              m[1]  * m[11] * m[14] + 
			              m[9]  * m[2] * m[15] - 
			              m[9]  * m[3] * m[14] - 
			              m[13] * m[2] * m[11] + 
			              m[13] * m[3] * m[10];

			    inv.m[5] = m[0]  * m[10] * m[15] - 
			             m[0]  * m[11] * m[14] - 
			             m[8]  * m[2] * m[15] + 
			             m[8]  * m[3] * m[14] + 
			             m[12] * m[2] * m[11] - 
			             m[12] * m[3] * m[10];

			    inv.m[9] = -m[0]  * m[9] * m[15] + 
			              m[0]  * m[11] * m[13] + 
			              m[8]  * m[1] * m[15] - 
			              m[8]  * m[3] * m[13] - 
			              m[12] * m[1] * m[11] + 
			              m[12] * m[3] * m[9];

			    inv.m[13] = m[0]  * m[9] * m[14] - 
			              m[0]  * m[10] * m[13] - 
			              m[8]  * m[1] * m[14] + 
			              m[8]  * m[2] * m[13] + 
			              m[12] * m[1] * m[10] - 
			              m[12] * m[2] * m[9];

			    inv.m[2] = m[1]  * m[6] * m[15] - 
			             m[1]  * m[7] * m[14] - 
			             m[5]  * m[2] * m[15] + 
			             m[5]  * m[3] * m[14] + 
			             m[13] * m[2] * m[7] - 
			             m[13] * m[3] * m[6];

			    inv.m[6] = -m[0]  * m[6] * m[15] + 
			              m[0]  * m[7] * m[14] + 
			              m[4]  * m[2] * m[15] - 
			              m[4]  * m[3] * m[14] - 
			              m[12] * m[2] * m[7] + 
			              m[12] * m[3] * m[6];

			    inv.m[10] = m[0]  * m[5] * m[15] - 
			              m[0]  * m[7] * m[13] - 
			              m[4]  * m[1] * m[15] + 
			              m[4]  * m[3] * m[13] + 
			              m[12] * m[1] * m[7] - 
			              m[12] * m[3] * m[5];

			    inv.m[14] = -m[0]  * m[5] * m[14] + 
			               m[0]  * m[6] * m[13] + 
			               m[4]  * m[1] * m[14] - 
			               m[4]  * m[2] * m[13] - 
			               m[12] * m[1] * m[6] + 
			               m[12] * m[2] * m[5];

			    inv.m[3] = -m[1] * m[6] * m[11] + 
			              m[1] * m[7] * m[10] + 
			              m[5] * m[2] * m[11] - 
			              m[5] * m[3] * m[10] - 
			              m[9] * m[2] * m[7] + 
			              m[9] * m[3] * m[6];

			    inv.m[7] = m[0] * m[6] * m[11] - 
			             m[0] * m[7] * m[10] - 
			             m[4] * m[2] * m[11] + 
			             m[4] * m[3] * m[10] + 
			             m[8] * m[2] * m[7] - 
			             m[8] * m[3] * m[6];

			    inv.m[11] = -m[0] * m[5] * m[11] + 
			               m[0] * m[7] * m[9] + 
			               m[4] * m[1] * m[11] - 
			               m[4] * m[3] * m[9] - 
			               m[8] * m[1] * m[7] + 
			               m[8] * m[3] * m[5];

			    inv.m[15] = m[0] * m[5] * m[10] - 
			              m[0] * m[6] * m[9] - 
			              m[4] * m[1] * m[10] + 
			              m[4] * m[2] * m[9] + 
			              m[8] * m[1] * m[6] - 
			              m[8] * m[2] * m[5];

			    det = m[0] * inv.m[0] + m[1] * inv.m[4] + m[2] * inv.m[8] + m[3] * inv.m[12];

			    if (det == 0) {
			    	throw std::runtime_error("Matrix is not invertible."); 
			    }

			    det = 1.0 / det;

			    for (i = 0; i < 16; i++)
			        inv[i] = inv[i] * det;

			    return inv;
			}

			Matrix4<TYPE> transpose() {
				return Matrix4<TYPE>(
					m[0], m[4], m[8], 	m[12],
					m[1], m[5], m[9], 	m[13],
					m[2], m[6], m[10], 	m[14],
					m[3], m[7], m[8], 	m[15]
				);
			}

			bool operator==(const Matrix4<TYPE> &other) {
				return m[0] == other.m[0] && m[1] == other.m[1] &&
						m[2] == other.m[2] && m[3] == other.m[3] &&
						m[4] == other.m[4] && m[5] == other.m[5] &&
						m[6] == other.m[6] && m[7] == other.m[7] &&
						m[8] == other.m[8] && m[9] == other.m[9] &&
						m[10] == other.m[10] && m[11] == other.m[11] &&
						m[12] == other.m[12] && m[13] == other.m[13] &&
						m[14] == other.m[14] && m[15] == other.m[15];
			}

			Matrix4<TYPE> round() {
				return Matrix4<TYPE>(
					std::round(m[0]), std::round(m[1]), std::round(m[2]), std::round(m[3]),
					std::round(m[4]), std::round(m[5]), std::round(m[6]), std::round(m[7]),
					std::round(m[8]), std::round(m[9]), std::round(m[10]), std::round(m[11]),
					std::round(m[12]), std::round(m[13]), std::round(m[14]), std::round(m[15])
				);
			}

			TYPE *data() {
				return m;
			}

			const TYPE *data() const {
				return m;
			}
	};

	using Matrix4f = Matrix4<float>;
	using Matrix4d = Matrix4<double>;
};

#endif