#ifndef G3D_MATH_H
#define G3D_MATH_H

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

#include <cmath>
#include <algorithm>

namespace G3D {

	namespace Math {

		template <typename TYPE>
		TYPE lerp(const TYPE a, const TYPE b, const TYPE t) {
			return a + t * (b - a);
		}

		template <typename TYPE>
		TYPE degreeToRadian(TYPE a) {
			return a * (TYPE) M_PI / 180;
		}

		template <typename T>
		T clamp(const T& n, const T& lower, const T& upper) {
		  return std::max(lower, std::min(n, upper));
		}
	};

};

#endif