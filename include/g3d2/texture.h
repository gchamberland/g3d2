#ifndef TEXTURE_H
#define TEXTURE_H

#include <SFML/Graphics/Image.hpp>
#include <GL/glew.h>

namespace G3D {

	class GLTexture2D {

		private:
			static int& currentTextureSlotIndex() { static int I = 0; return I; }

			GLuint glId;
			int slotIndex;

		public:
			GLTexture2D() {
				glId = 0;
				slotIndex = -1;
			}

			GLTexture2D(const GLTexture2D &other) {
				glId = other.glId;
				slotIndex = other.slotIndex;
			}

			GLTexture2D(const std::string &filename) {
				slotIndex = -1;
				sf::Image image;
				image.loadFromFile(filename);

				glGenTextures(1, &glId);
				glBindTexture(GL_TEXTURE_2D, glId);

				// Set filtering
		        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		        glTexImage2D(
		        	GL_TEXTURE_2D,
		        	0,
		        	GL_RGBA,
		        	image.getSize().x,
		        	image.getSize().y,
		        	0,
		        	GL_RGBA,
		        	GL_UNSIGNED_BYTE,
		        	image.getPixelsPtr()
	        	);

				glBindTexture(GL_TEXTURE_2D, 0);
			}


			void bind() {
		        if (slotIndex != -1) {
		            throw std::runtime_error("Texture is already bound.");
		        }

		        if (glId == 0) {
		        	throw std::runtime_error("Texture is empty.");
		        }

		        slotIndex = currentTextureSlotIndex()++;
		        glActiveTexture(GL_TEXTURE0 + slotIndex);
		        glBindTexture(GL_TEXTURE_2D, glId);
		    }

		    void unbind() {
		        if (slotIndex == -1) {
		            throw std::runtime_error("Texture is not bound.");
		        }

		        glActiveTexture(GL_TEXTURE0 + slotIndex);
		        glBindTexture(GL_TEXTURE_2D, 0);
		        slotIndex = -1;
		        --currentTextureSlotIndex();
		    }

		    int getTextureSlotIndex() {
		        if (slotIndex == -1) {
		            throw std::runtime_error("Texture is not bound.");
		        }
		        return slotIndex;
		    }

		    void release() {
	            if (glId == 0) {
	                throw std::runtime_error("Texture is empty.");
	            }
	            glDeleteTextures(1, &glId);
	        }
	};

};

#endif