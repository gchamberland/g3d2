#ifndef GL_BUFFER_H
#define GL_BUFFER_H

#include <GL/glew.h>
#include <cstdint>

#include <stdexcept>

namespace G3D {

    template <GLuint BUFFER_TYPE>
    class GLBuffer {
        private:
            GLuint glId;

        public:

        GLBuffer() {
            glId = 0;
        }

        GLBuffer(const GLBuffer<BUFFER_TYPE> &other) {
            glId = other.glId;
        }

        GLBuffer(size_t sizeInByte, GLuint bufferUsage = GL_STATIC_DRAW)
            :GLBuffer((uint8_t *)NULL, sizeInByte, 0, bufferUsage){}

        template <typename TYPE>
        GLBuffer(const TYPE *data, size_t length, size_t offset = 0, int bufferUsage = GL_STATIC_DRAW)
        {
            glGenBuffers(1, &glId);

            if (glGetError() != GL_NO_ERROR) {
                throw std::runtime_error("Error creating the buffer.");
            }

            glBindBuffer(BUFFER_TYPE, glId);

                glBufferData(BUFFER_TYPE, length * sizeof(TYPE), (void *) (data + offset), bufferUsage);

            glBindBuffer(BUFFER_TYPE, 0);

            if (glGetError() == GL_OUT_OF_MEMORY) {
                throw std::runtime_error("Not enough video memory.");
            }
        }

        void bind() {
            if (glId == 0) {
                throw std::runtime_error("Buffer is not initialised.");
            }

            glBindBuffer(BUFFER_TYPE, glId);
        }

        void unbind() {
            if (glId == 0) {
                throw std::runtime_error("Buffer is not initialised.");
            }

            glBindBuffer(BUFFER_TYPE, 0);
        }

        template <typename TYPE>
        void uploadData(const TYPE *data, int length, int offset = 0) {
            if (glId == 0) {
                throw std::runtime_error("Buffer is not initialised.");
            }

            glBindBuffer(BUFFER_TYPE, glId);
                glBufferSubData(BUFFER_TYPE, offset,  length * sizeof(TYPE), data);
            glBindBuffer(BUFFER_TYPE, 0);
        }

        void release() {
            if (glId == 0) {
                throw std::runtime_error("Buffer is empty.");
            }
            glDeleteBuffers(1, &glId);
        }
    };

    using GLVertexBuffer = GLBuffer<GL_ARRAY_BUFFER>;
    using GLElementBuffer = GLBuffer<GL_ELEMENT_ARRAY_BUFFER>;
};

#endif