#ifndef VECTOR_H
#define VECTOR_H

#include <algorithm>

namespace G3D {

	template <typename TYPE>
	class Vector4;

	template <typename TYPE>
	class Vector3 {
		private:
			TYPE v[3];

		public:
			static Vector3<TYPE> zero() {
				return Vector3<TYPE>(0, 0, 0);
			}

			static Vector3<TYPE> one() {
				return Vector3<TYPE>(1, 1, 1);
			}

			static Vector3<TYPE> right() {
				return Vector3<TYPE>(1, 0, 0);
			}

			static Vector3<TYPE> left() {
				return Vector3<TYPE>(-1, 0, 0);
			}

			static Vector3<TYPE> up() {
				return Vector3<TYPE>(0, 1, 0);
			}

			static Vector3<TYPE> down() {
				return Vector3<TYPE>(0, -1, 0);
			}

			static Vector3<TYPE> forward() {
				return Vector3<TYPE>(0, 0, 1);
			}

			static Vector3<TYPE> back() {
				return Vector3<TYPE>(0, 0, -1);
			}

			Vector3() {}

			Vector3(const Vector3<TYPE> &other) {
				std::copy(&other.v[0], &other.v[3], &v[0]);
			}

			Vector3(TYPE x, TYPE y, TYPE z) {
				v[0] = x; v[1] = y; v[2] = z;
			}

			TYPE &x() {
				return v[0];
			}

			TYPE &y() {
				return v[1];
			}

			TYPE &z() {
				return v[2];
			}

			const TYPE &x() const {
				return v[0];
			}

			const TYPE &y() const {
				return v[1];
			}

			const TYPE &z() const {
				return v[2];
			}

			TYPE &operator[](std::size_t index) {
				return v[index];
			}

			const TYPE &operator[](std::size_t index) const {
				return v[index];
			}

			Vector3<TYPE> operator*(const TYPE &scalar) const {
				return Vector3<TYPE>(v[0] * scalar, v[1] * scalar, v[2] * scalar);
			}

			Vector3<TYPE> operator/(const TYPE &scalar) const {
				return Vector3<TYPE>(v[0] / scalar, v[1] / scalar, v[2] / scalar);
			}

			Vector3<TYPE> operator+(const Vector3<TYPE> &other) const {
				return Vector3<TYPE>(v[0] + other->v[0], v[1] + other->v[1], v[2] + other->v[2]);
			}

			Vector3<TYPE> operator-(const Vector3<TYPE> &other) const {
				return Vector3<TYPE>(v[0] - other->v[0], v[1] - other->v[1], v[2] - other->v[2]);
			}

			TYPE dot(const Vector3<TYPE> &other) const {
				return v[0] * other->v[0] + v[1] * other->v[1] + v[2] * other->v[2];
			}

			Vector3<TYPE> cross(const Vector3<TYPE> &other) const {
				return Vector3<TYPE>(
					v[1] * other->v[2] - v[2] * other->v[1],
					v[2] * other->v[0] - v[0] * other->v[2],
					v[0] * other->v[1] - v[0] * other->v[1]
				);
			}

			TYPE sqrMagnitude() {
				return v[0]*v[0] + v[1]*v[1] + v[2]*v[2];
			}

			TYPE magnitude() {
				return std::sqrt(sqrMagnitude());
			}

			Vector3<TYPE> normalized() {
				TYPE magnitude = magnitude();
				return Vector3<TYPE>(
					v[0] / magnitude,
					v[1] / magnitude,
					v[2] / magnitude
				);
			}

			Vector4<TYPE> toHomogenousPoint() {
				return Vector4<TYPE>(v[0], v[1], v[2], 1.0);
			}

			Vector4<TYPE> toHomogenousVector() {
				return Vector4<TYPE>(v[0], v[1], v[2], 0.0);
			}

			TYPE *data() {
				return v;
			}

			const TYPE *data() const {
				return v;
			}
	};

	template <typename TYPE>
	class Vector4 {
		private:
			TYPE v[4];

		public:
			static Vector4<TYPE> zero() {
				return Vector4<TYPE>(0, 0, 0);
			}

			static Vector4<TYPE> one() {
				return Vector4<TYPE>(1, 1, 1);
			}

			Vector4() {}

			Vector4(const Vector4<TYPE> &other) {
				std::copy(&other.v[0], &other.v[4], &v[0]);
			}

			Vector4(TYPE x, TYPE y, TYPE z, TYPE w) {
				v[0] = x; v[1] = y; v[2] = z; v[3] = w;
			}

			TYPE &x() {
				return v[0];
			}

			TYPE &y() {
				return v[1];
			}

			TYPE &z() {
				return v[2];
			}

			TYPE &w() {
				return v[3];
			}

			const TYPE &x() const {
				return v[0];
			}

			const TYPE &y() const {
				return v[1];
			}

			const TYPE &z() const {
				return v[2];
			}

			const TYPE &w() const {
				return v[3];
			}

			TYPE &operator[](std::size_t index) {
				return v[index];
			}

			const TYPE &operator[](std::size_t index) const {
				return v[index];
			}

			Vector4<TYPE> operator*(const TYPE &scalar) const {
				return Vector4<TYPE>(v[0] * scalar, v[1] * scalar, v[2] * scalar, v[3] * scalar);
			}

			Vector4<TYPE> operator/(const TYPE &scalar) const {
				return Vector4<TYPE>(v[0] / scalar, v[1] / scalar, v[2] / scalar, v[3] / scalar);
			}

			Vector4<TYPE> operator+(const Vector4<TYPE> &other) const {
				return Vector3<TYPE>(v[0] + other->v[0], v[1] + other->v[1], v[2] + other->v[2], v[3] + other->v[3]);
			}

			Vector4<TYPE> operator-(const Vector4<TYPE> &other) const {
				return Vector3<TYPE>(v[0] - other->v[0], v[1] - other->v[1], v[2] - other->v[2], v[3] - other->v[3]);
			}

			TYPE dot(const Vector4<TYPE> &other) const {
				return v[0] * other->v[0] + v[1] * other->v[1] + v[2] * other->v[2] + v[3] * other->v[3];
			}

			TYPE sqrMagnitude() {
				return v[0]*v[0] + v[1]*v[1] + v[2]*v[2] + v[3]*v[3];
			}

			TYPE magnitude() {
				return std::sqrt(sqrMagnitude());
			}

			Vector4<TYPE> normalized() {
				TYPE magnitude = magnitude();
				return Vector3<TYPE>(
					v[0] / magnitude,
					v[1] / magnitude,
					v[2] / magnitude,
					v[3] / magnitude
				);
			}

			TYPE *data() {
				return v;
			}

			const TYPE *data() const {
				return v;
			}
	};

	using Vector3f = Vector3<float>;
	using Vector3d = Vector3<double>;
	using Vector4f = Vector4<float>;
	using Vector4d = Vector3<double>;
}

#endif