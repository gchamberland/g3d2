#ifndef GL_PROGRAM_H
#define GL_PROGRAM_H

#include <GL/glew.h>

#include <vector>
#include <map>
#include <string>
#include <iostream>

namespace G3D {
    
    class GLProgram {

        public:
            struct ProgramParameter {
                int location;
                int size;
                GLenum type;
            };

        private:
            std::vector<GLuint> shaderHandles;
            GLuint programHandle;
            std::map<const std::string, ProgramParameter> attributesInfos;
            std::map<const std::string, ProgramParameter> uniformsInfos;

            void initFromSources(const std::map<GLuint, std::string> &shadersSources) {
                for (const auto &entry : shadersSources) {
                    shaderHandles.push_back(buildShader(entry.first, entry.second));
                }

                this->programHandle = createProgram(shaderHandles);
                this->attributesInfos = getProgramParameters(this->programHandle, ATTRIBUTE);
                this->uniformsInfos = getProgramParameters(this->programHandle, UNIFORM);
            }

        public:

            enum ProgramParameterType {
                UNIFORM,
                ATTRIBUTE
            };

            static GLuint buildShader(GLuint shaderType, const std::string &source) {
                GLuint shaderHandle = glCreateShader(shaderType);

                if (shaderHandle == 0) {
                    throw std::runtime_error("Invalid shader type");
                }

                const GLchar *sourcePtr = (const GLchar *) source.c_str();
                glShaderSource(shaderHandle, 1, &sourcePtr, NULL);
                glCompileShader(shaderHandle);
                int compiled;
                glGetShaderiv(shaderHandle, GL_COMPILE_STATUS, &compiled);
                if (compiled == 0) {
                    char errorMessage[256];
                    glGetShaderInfoLog(shaderHandle, 256, NULL, errorMessage);
                    glDeleteShader(shaderHandle);
                    std::cout << errorMessage << std::endl;
                    throw std::runtime_error(errorMessage);
                }

                return shaderHandle;
            }

            static GLuint createProgram(const std::vector<GLuint> &shadersHandles) {
                GLuint program = glCreateProgram();

                if (program == 0) {
                    throw std::runtime_error("Can't create GLSL program");
                }

                for (const GLuint &shaderHandle : shadersHandles) {
                    glAttachShader(program, shaderHandle);
                }

                glLinkProgram(program);
                int linked;
                glGetProgramiv(program, GL_LINK_STATUS, &linked);
                if (linked != GL_TRUE) {
                    char errorMessage[256];
                    glGetProgramInfoLog(program, 256, NULL, errorMessage);
                    glDeleteProgram(program);
                    std::cout << errorMessage << std::endl;
                    throw std::runtime_error(errorMessage);
                }
                return program;
            }


            static std::map<const std::string, ProgramParameter> getProgramParameters(
                    GLuint program, ProgramParameterType type) {
                char name[256];
                int paramCount;
                std::map<const std::string, ProgramParameter> results;

                glGetProgramiv(
                        program,
                        type == ATTRIBUTE ?
                                GL_ACTIVE_ATTRIBUTES :
                                GL_ACTIVE_UNIFORMS,
                        &paramCount
                );

                for (int i=0; i<paramCount; i++) {
                    ProgramParameter param;
                    switch (type) {
                        case ATTRIBUTE:
                            glGetActiveAttrib(program, i, 256, NULL, &param.size, &param.type, name);
                        break;
                        
                        case UNIFORM:
                            glGetActiveUniform(program, i, 256, NULL, &param.size, &param.type, name);
                        break;
                    }

                    param.location = type == ATTRIBUTE ?
                        glGetAttribLocation(program, name) :
                        glGetUniformLocation(program, name);

                    results[std::string(name)] = param;
                }

                return results;
            }

            GLProgram() {
            }

            GLProgram(const std::string &vertexShader, const std::string &fragmentShader) {
                std::map<GLuint, std::string> shadersSources;

                shadersSources[GL_VERTEX_SHADER] = vertexShader;
                shadersSources[GL_FRAGMENT_SHADER] = fragmentShader;

                initFromSources(shadersSources);
            }

            GLProgram(const std::map<GLuint, std::string> &shadersSources) {
                initFromSources(shadersSources);
            }

            void release() {
                attributesInfos.clear();
                uniformsInfos.clear();

                glDeleteProgram(programHandle);
                programHandle = 0;

                for (const int handle : shaderHandles) {
                    glDeleteShader(handle);
                }
            }

            void use() {
                glUseProgram(programHandle);
            }

            const ProgramParameter &getUniform(const std::string &name) {
                return uniformsInfos[name];
            }

            const ProgramParameter &getAttribut(const std::string &name) {
                return attributesInfos[name];
            }
    };
};

#endif